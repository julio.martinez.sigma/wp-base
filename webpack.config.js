const path = require("path");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

module.exports = {
  entry: {
    app: ["./assets/app.js", "./assets/styles/style.scss"],
  },
  output: {
    filename: '[name].min.js',  // renames files from example.js to example.8f77someHash8adfa.js
    path: path.resolve(__dirname, './build/js'), // output to BASE_DIR/dist, assumes webpack.json is on the same level as manage.py
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          MiniCssExtractPlugin.loader,
          {
            "loader":"css-loader",
            "options":{
               sourceMap: true
            }
         },
         {
            "loader":"sass-loader"
         },
        ],
      }
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "../../style.css"
    }),
    new CleanWebpackPlugin()
  ],
  resolve: {
    extensions: ["*", ".js",".json"],
  },
  optimization: {
    runtimeChunk: "single",
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendors",
          priority: -10,
          chunks: "all",
        },
      },
    },
  },
};